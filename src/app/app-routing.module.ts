import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BookComponent } from './content/book/book.component'
import { CategoryComponent } from './content/category/category.component'
import { HomeComponent } from './content/home/home.component'


const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
    pathMatch: 'full',
  },{
    path: "category/:id",
    component: CategoryComponent
  },{
    path: "book/:id",
    component: BookComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
