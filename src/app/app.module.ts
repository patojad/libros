import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TopbarComponent } from './layouts/partials/topbar/topbar.component';
import { FooterComponent } from './layouts/partials/footer/footer.component';
import { CardComponent } from './layouts/static/card/card.component';
import { BookComponent } from './content/book/book.component';
import { CategoryComponent } from './content/category/category.component';
import { HomeComponent } from './content/home/home.component';
import { LateralbarComponent } from './layouts/partials/lateralbar/lateralbar.component';
import { DonateComponent } from './layouts/static/widgets/donate/donate.component';

@NgModule({
  declarations: [
    AppComponent,
    TopbarComponent,
    FooterComponent,
    CardComponent,
    BookComponent,
    CategoryComponent,
    HomeComponent,
    LateralbarComponent,
    DonateComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
