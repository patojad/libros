import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { API } from '../../../services/api';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss']
})
export class CategoryComponent implements OnInit {
  category_books = []
  url = "?category_id="

  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
    console.log()
    this.route.params.subscribe(params => {
      this.getCategory(params['id']); // reset and set based on new parameter this time
    });
  }

  // ?criteria=most_viewed
  getCategory(id_cat){
    API.get(this.url.concat(id_cat).concat('&results_range=0,12'))
      .then(res =>{
        this.category_books = res
      })
      .catch(err =>{
        console.log(err)
      })
  }


}
