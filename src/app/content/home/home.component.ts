import { Component, OnInit } from '@angular/core';

import { API } from '../../../services/api';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  categories = []
  most_viewed = []

  constructor() { }

  ngOnInit() {
    this.getMostViewed();
  }

  // ?criteria=most_viewed
  getMostViewed(){
    API.get('?criteria=most_viewed&results_range=0,12')
      .then(res =>{
        this.most_viewed = res
      })
      .catch(err =>{
        console.log(err)
      })
  }


}
