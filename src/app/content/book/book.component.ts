import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { API } from '../../../services/api';

@Component({
  selector: 'app-book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.scss']
})
export class BookComponent implements OnInit {
  books = []
  url = "?id="

  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.getBook(params['id']); // reset and set based on new parameter this time
    });
  }

  // ?criteria=most_viewed
  getBook(id_book){
    API.get(this.url.concat(id_book))
      .then(res =>{
        this.books= res;
      })
      .catch(err =>{
        console.log(err)
      })
  }


}
