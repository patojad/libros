import { Component, OnInit } from '@angular/core';

import { API } from '../../../../services/api';

@Component({
  selector: 'app-topbar',
  templateUrl: './topbar.component.html',
  styleUrls: ['./topbar.component.scss']
})
export class TopbarComponent implements OnInit {
  categories = [];

  constructor() { }

  ngOnInit() {
    this.getCategories();
  }

  getCategories(){
    API.get('?get_categories=all')
      .then(res =>{
        this.categories = res;
      })
      .catch(err =>{
        console.log(err);
      })
  }
}
