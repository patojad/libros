import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {
  @Input() id;
  @Input() titulo: string;
  @Input() contenido: string;
  @Input() img: string;
  @Input() autor: string;
  @Input() comentarios: number;
  @Input() fecha;
  @Input() paginas: number;

  constructor() { }

  ngOnInit(): void {
  }

}
