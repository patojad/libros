import axios from 'axios';

const server = 'https://www.etnassoft.com/api/v1/get/';
const aditionalparams= '&json=true&decode=true';

export const API = {
  get: path => axios.get(`${server}${path}${aditionalparams}`)
  .then(response => response.data),
};
